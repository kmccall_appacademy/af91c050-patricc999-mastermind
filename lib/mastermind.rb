class Code

  PEGS = { r: "Red",
           g: "Green",
           b: "Blue",
           y: "Yellow",
           p: "Purple",
           o: "Orange" }.freeze

  attr_reader :pegs

  def initialize(pegs)
    if pegs.class == Array
      @pegs = pegs
    else
      raise ArgumentError
    end
  end

  def self.parse(code)
    valid_pegs = PEGS.keys.map(&:to_s)
    pegs = code.downcase.split('')
    pegs.each do |peg|
      if !valid_pegs.include?(peg)
        raise 'Invalid Color'
      end
    end

    Code.new(pegs)
  end

  def self.random
    peg_abbrevs = PEGS.keys
    random_pegs = []
    4.times do
      random_pegs << peg_abbrevs[get_random_num]
    end

    Code.new(random_pegs)
  end

  def self.get_random_num
    prng = Random.new
    prng.rand(0..5)
  end

  def [](index)
    pegs[index]
  end

  def exact_matches(codes)
    exact_matches = 0
    codes.pegs.each_with_index do |code, idx|
      if code == self.pegs[idx]
        exact_matches += 1
      end
    end
    exact_matches
  end

  def near_matches(codes)
    near_matches = 0
    codes.pegs.each_with_index do |code, idx|
      if self.pegs.count(code) > near_matches && code != self.pegs[idx]
        near_matches += 1
      end
    end
    near_matches
  end

  def ==(guess)
    if guess.class != Code
      return false
    end
    if exact_matches(guess) == 4
      true
    end
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts 'Please make a guess'
    guess = gets.chomp
    Code.parse(guess)
  end

  def display_matches(code)
    puts 'exact matches: ' + secret_code.exact_matches(code).to_s
    puts 'near matches: ' + secret_code.near_matches(code).to_s
  end
end
